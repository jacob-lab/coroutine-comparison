package ch.frankel.blog.coroutine

class DummyService(private val name: String) {

    // private val random = SecureRandom()

    val content: ContentDuration
        get() {
            val duration = 10000 // random.nextInt(10000)
            Thread.sleep(duration.toLong())
            return ContentDuration(name, duration)
        }
}

data class ContentDuration(val content: String, val duration: Int)
